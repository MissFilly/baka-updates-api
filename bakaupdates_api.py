# bakaupdates_api.py

import falcon
import requests
import json
from urllib import parse
from bs4 import BeautifulSoup


class BaseResource(object):
    base_url = 'https://www.mangaupdates.com/'

    @property
    def url_path(self):
        raise NotImplementedError('Resource class must have attribute "url_path".')

    def _parse_content(self, soup, req):
        raise NotImplementedError('Resource class must have a parsing method.')

    def on_get(self, req, resp):
        # try:
        params = req.params
        r = requests.get(self.base_url + self.url_path, params)
        resp.status = falcon.HTTP_200
        soup = BeautifulSoup(r.text, features='html.parser')
        body = self._parse_content(soup, req)
        resp.body = (json.dumps(body))
        # except Exception:
        #     # TODO: clean
        #     resp.status = falcon.HTTP_500
        #     resp.body = (json.dumps({'message': 'Unknown error'}))


class SeriesResource(BaseResource):
    url_path = 'series.html'

    def _parse_list_page(self, soup):
        headers = soup.findAll('td', {'class': 'releasestitle'})
        titles = soup.findAll('a', alt='Series Info')
        attributes = [header.text.lower() for header in headers]
        body = []

        for title in titles:
            parent = title.parent.parent
            children = parent.findAll('td')

            # Add ID to record
            detail_url = title['href']
            query_params = dict(parse.parse_qsl(parse.urlsplit(detail_url).query))
            data = {'id': query_params['id']}

            # TODO: add URL to detail endpoint

            # Iterate over cells in row
            for idx, child in enumerate(children):
                attribute = attributes[idx]
                value = child.text
                if attribute == 'genre':
                    genres = []
                    for genre in value.split(','):
                        if '...' in genre:
                            continue
                        genres.append(genre.strip())
                    data[attribute] = genres
                else:
                    data[attribute] = value
            body.append(data)

        return body

    def _parse_detail(self, soup):
        attributes = soup.findAll('div', {'class': 'sCat'})
        data = []
        for attribute in attributes:
            name = attribute.text
            content = attribute.findNext('div', {'class': 'sContent'})
            data.append({
                'name': name,
                'value': list(content.stripped_strings)
            })
        return data

    def _parse_content(self, soup, req):
        """
        The series.html page's content changes depending
        on the parameters it receives.

        :param soup: BS4 parser
        :param req: falcon Request object
        :return: dictionary
        """
        params = req.params
        if 'id' in params:
            callback = self._parse_detail
        elif 'act' in params:
            callback = None  # parse genre search page
        else:
            callback = self._parse_list_page
        return callback(soup)


class GenresResource(BaseResource):
    url_path = 'genres.html'

    def _parse_content(self, soup, req):
        content = soup.find('td', {'id': 'main_content'})
        genres = content.findAll('releasestitle')
        # TODO: add description, stats
        return [genre.text for genre in genres]


app = falcon.API()

app.add_route('/series', SeriesResource())
app.add_route('/genres', GenresResource())
